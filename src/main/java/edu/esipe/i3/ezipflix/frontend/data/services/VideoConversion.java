package edu.esipe.i3.ezipflix.frontend.data.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import edu.esipe.i3.ezipflix.frontend.ConversionRequest;
import edu.esipe.i3.ezipflix.frontend.ConversionResponse;
import edu.esipe.i3.ezipflix.frontend.data.entities.VideoConversions;
import edu.esipe.i3.ezipflix.frontend.data.repositories.VideoConversionRepository;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;

/**
 * [Service] VideoConversion
 * Created by Gilles GIRAUD gil on 11/4/17.
 * Edited by Jessica & Angélique
 */
@Service
public class VideoConversion {

    @Value("${conversion.messaging.rabbitmq.conversion-queue}")
    public String conversionQueue;

    @Value("${conversion.messaging.rabbitmq.conversion-exchange}")
    public String conversionExchange;

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Autowired
    VideoConversionRepository videoConversionRepository;

    @Autowired
    VideoConversionPublisher videoConversionPublisher;

    @Autowired
    VideoConversionDataStore videoConversionDataStore;

    @Autowired
    VideoConversionStorage videoConversionStorage;

    public VideoConversion() {
    }

    public void save(final ConversionRequest request, final ConversionResponse response) throws JsonProcessingException {

        final VideoConversions conversion = new VideoConversions(
                response.getUuid().toString(),
                request.getPath().toString(),
                "");
        String message = conversion.toJson();

        // Publish message on google pubsub
        try {
            videoConversionPublisher.publish(message);
        } catch (Exception e) {
            System.err.println("error publishing: " + e);
        }

        // Upload file on google storage
        try {
        videoConversionStorage.uploadFile(new File(request.getPath()));
        } catch (Exception e) {
            System.err.println("error uploading: " + e);
        }


        // Save conversion on google datastore
        videoConversionDataStore.store("conversion", "conversion-string", message);





    }

}
