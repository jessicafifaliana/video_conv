package edu.esipe.i3.ezipflix.frontend.data.services;

import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.DatastoreOptions;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.Key;
import org.springframework.stereotype.Service;

/**
 * [Service] VideoConversionDataStore
 * @author Jessica & Angélique
 */
@Service
public class VideoConversionDataStore {

    private Datastore datastore;
    private Entity entity;
    private String kind = "video-conversion";

    public VideoConversionDataStore() {
        this.datastore = DatastoreOptions.getDefaultInstance().getService();
    }

    public void store (String name, String key, String value) {
        Key entityKey = this.datastore.newKeyFactory().setKind(kind).newKey(name);
        this.entity = Entity.newBuilder(entityKey).set(key, value).build();
        datastore.put(entity);
        System.out.printf("Saved %s: %s%n", entity.getKey().getName(), entity.getString(key));
    }

}
