package edu.esipe.i3.ezipflix.frontend.data.services;

import com.google.api.core.ApiFuture;
import com.google.api.core.ApiFutures;
import com.google.cloud.ServiceOptions;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.ProjectTopicName;
import com.google.pubsub.v1.PubsubMessage;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * [Service] VideoConversionPublisher
 * @author Jessica & Angélique
 */
@Service
public class VideoConversionPublisher {
    // use the default project id
    private static final String PROJECT_ID = ServiceOptions.getDefaultProjectId();
    //@Value("${conversion.messaging.cloud.topic-id}")
    private String topicId = "video_conversion";
    private ProjectTopicName topicName;
    private Publisher publisher;
    private List<ApiFuture<String>> futures;
    private List<String> messageIds;


    public VideoConversionPublisher() throws IOException {
        this.topicName = ProjectTopicName.of(PROJECT_ID, topicId);
        this.publisher = Publisher.newBuilder(this.topicName).build();
    }

    /**
     * Publish messages to a topic
     *
     * @param message
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public void publish(String message) throws ExecutionException, InterruptedException {
        futures = new ArrayList<>();
        try {
            // convert message to bytes
            ByteString data = ByteString.copyFromUtf8(message);
            PubsubMessage pubsubMessage = PubsubMessage.newBuilder()
                    .setData(data)
                    .build();

            // Schedule a message to be published. Messages are automatically batched.
            ApiFuture<String> future = publisher.publish(pubsubMessage);
            futures.add(future);
        } finally {
            // Wait on any pending requests
            messageIds = ApiFutures.allAsList(futures).get();

            for (String messageId : messageIds) {
                System.out.println(messageId);
            }


           /*if (publisher != null) {
                // When finished with the publisher, shutdown to free up resources.
                try {
                    publisher.shutdown();
                } catch (Exception e) {
                    System.out.println("Closing Google Cloud Publisher");
                }
            }*/
        }
    }

}