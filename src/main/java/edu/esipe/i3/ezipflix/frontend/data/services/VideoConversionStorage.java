package edu.esipe.i3.ezipflix.frontend.data.services;

import com.google.cloud.storage.Acl;
import com.google.cloud.storage.Acl.Role;
import com.google.cloud.storage.Acl.User;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Copyright 2016 Google Inc.
 */
@Service
public class VideoConversionStorage {

    private static Storage storage = null;

    // [START init]
    static {
        storage = StorageOptions.getDefaultInstance().getService();
    }

    //private String bucketName = "staging.cellular-gift-257815.appspot.com";
    private String bucketName = "jess7611.appspot.com";
    // [END init]

    // [START uploadFile]

    /**
     * Uploads a file to Google Cloud Storage to the bucket specified in the BUCKET_NAME
     * environment variable, appending a timestamp to end of the uploaded filename.
     */
    // Note: this sample assumes small files are uploaded. For large files or streams use:
    // Storage.writer(BlobInfo blobInfo, Storage.BlobWriteOption... options)
    public String uploadFile(File filePart) throws IOException {
        DateTimeFormatter dtf = DateTimeFormat.forPattern("-YYYY-MM-dd-HHmmssSSS");
        DateTime dt = DateTime.now(DateTimeZone.UTC);
        String dtString = dt.toString(dtf);
        final String fileName = filePart.getName() + dtString;

        // The InputStream is closed by default, so we don't need to close it here
        // Read InputStream into a ByteArrayOutputStream.
        InputStream is = new FileInputStream(filePart) ;//filePart.getInputStream();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        byte[] readBuf = new byte[4096];
        while (is.available() > 0) {
            int bytesRead = is.read(readBuf);
            os.write(readBuf, 0, bytesRead);
        }

        // Convert ByteArrayOutputStream into byte[]
        BlobInfo blobInfo =
                storage.create(
                        BlobInfo
                                .newBuilder(bucketName, fileName)
                                // Modify access list to allow all users with link to read file
                                .setAcl(new ArrayList<>(Arrays.asList(Acl.of(User.ofAllUsers(), Role.READER))))
                                .build(),
                        os.toByteArray());
        // return the public download link
        return blobInfo.getMediaLink();
    }
    // [END uploadFile]

}